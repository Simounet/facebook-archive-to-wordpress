<?php
/**
 * Plugin Name: Facebook Export 2 WordPress
 * Plugin URI:
 * Description: Import your Facebook exported posts.
 * Version: 1.0.0
 * Author: Simounet
 * Author URI: https://www.simounet.net
 * License: GPL2
 */

if ( !class_exists( 'FacebookExport2Wp' ) ) {
    class FacebookExport2Wp {
        const PAGE_LINK = 'import-facebook-posts';
        const INPUT_FILE = 'facebook-archive';
        const UPLOAD_DIR = '/facebookexport2wp';
        const DATA_EXTRACTED_FOLDER = '/datas';
        const POSTS_FILE = '/posts/your_posts_1.json';
        const DONE_FILE = '/done';
        const BUTTON_NAME = 'fb2wp-import';
        const IMPORT_AS_DRAFT_ID = 'fb2wp-import-as-draft';

        public static function init() {
            if(!is_admin()) {
                return false;
            }
            add_action( 'admin_init', ['FacebookExport2Wp', 'facebookExport2WpStyleClass']);
            add_action('admin_menu', ['FacebookExport2Wp', 'facebookExport2WpPluginMenu']);
            add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), ['FacebookExport2Wp', 'add_action_links']);
            register_activation_hook( __FILE__, ['FacebookExport2Wp', 'activation']);
            register_deactivation_hook( __FILE__, ['FacebookExport2Wp', 'deactivation']);
            add_action('admin_notices', ['FacebookExport2Wp', 'sample_admin_notice__success']);
        }

        public static function sample_admin_notice__success() {
            $screen = get_current_screen();
            $uploadDir = self::getUploadDir();
            $doneFile = self::getDoneFile($uploadDir);
            if(!file_exists($doneFile) || $screen->id === 'tools_page_' . self::PAGE_LINK) {
                return false;
            }
            ?>
            <div class="notice notice-warning">
                <h2>Facebook Export 2 WordPress Extension</h2>
                <?php self::printFinished(); ?>
            </div>
            <?php
        }

        public static function activation() {
            $wpUploadDir = wp_upload_dir();
            wp_mkdir_p($wpUploadDir['basedir'] . self::UPLOAD_DIR);
        }

        public static function deactivation() {
            $wpUploadDir = wp_upload_dir();
            self::delTree($wpUploadDir['basedir'] . self::UPLOAD_DIR);
        }

        protected static function delTree($dir) {
            $files = array_diff(scandir($dir), array('.','..'));
            foreach ($files as $file) {
                is_dir("$dir/$file") ? self::delTree("$dir/$file") : unlink("$dir/$file");
            }
            return rmdir($dir);
        }

        public static function add_action_links ( $links ) {
            $pluginLinks = array(
                '<a href="' . admin_url( 'tools.php?page=' . self::PAGE_LINK ) . '">Settings</a>',
            );
            return array_merge( $links, $pluginLinks );
        }

        public function facebookExport2WpStyleClass() {
            wp_register_style('stylesheet', plugins_url('public/css/styles.css', __FILE__ ));
            wp_enqueue_style('stylesheet');
        }

        public function facebookExport2WpPluginMenu() {
            add_submenu_page('tools.php', 'Import Facebook posts', 'Import Facebook posts', 'administrator', self::PAGE_LINK, ['FacebookExport2Wp', 'facebookExport2WpInit']);
        }

        public function facebookExport2WpInit() {
            echo '<h1>Import Facebook posts</h1>';
            $uploadDir = self::getUploadDir();
            $datasDir = $uploadDir . self::DATA_EXTRACTED_FOLDER;
            try {
                require_once(__DIR__ . '/includes/FacebookExport2WpZip.php');
                $zip = new FacebookExport2WpZip($uploadDir, $datasDir);
                $zipFileExtracted = $zip->fileHandling(self::BUTTON_NAME);
                if($zipFileExtracted) {
                    $doneFile = self::getDoneFile($uploadDir);
                    if(!file_exists($doneFile)) {
                        $dataExtracted = self::extractData($uploadDir, $datasDir, $doneFile);
                    }
                    self::printFinished();
                }
            } catch(Exception $e) {
                if( $e->getCode() === $zip::UNAUTHORIZED) {
                    self::printProcessForm($e->getMessage());
                } else {
                    self::printNoticeError($e->getMessage(), false); exit;
                }
            }
        }

        private static function getUploadDir()
        {
            $wpUploadDir = wp_upload_dir();
            return $wpUploadDir['basedir'] . self::UPLOAD_DIR;
        }

        private static function getDoneFile($uploadDir)
        {
            return $uploadDir . self::DONE_FILE;
        }

        private function printFinished()
        {
            ?>
            <p>Your datas seems to be properly imported. You should <a href="<?php echo admin_url('plugins.php?plugin_status=active'); ?>">disable and remove this extension</a>. It will safely remove unecessary files.</p>
            <?php
        }

        private static function extractData($uploadDir, $datasDir, $doneFile)
        {
            $jsonFile = $datasDir . self::POSTS_FILE;
            if(!file_exists($jsonFile)) {
                throw new Exception('Missing file: ' . $jsonFile);
            }
            $json = file_get_contents($jsonFile);
            require_once(__DIR__ . '/includes/FacebookExport2WpImport.php');
            $fb2wpImport = new FacebookExport2WpImport($uploadDir, $datasDir);
            $importAsDrafts = isset($_POST[self::IMPORT_AS_DRAFT_ID]);
            $count = $fb2wpImport->importPosts($json, $importAsDrafts);
            if($count > 0) {
                self::printNoticeSuccess('Successfully uploaded ' . $count . ' posts.');
                touch($doneFile);
                return true;
            }
            throw new Exception('Missing <code>done</code> file.');
        }

        protected static function printNoticeError($content, $dismissible = true) {
            self::printNotice($content, ' notice-error', $dismissible);
        }

        protected static function printNoticeSuccess($content, $dismissible = true) {
            self::printNotice($content, ' updated', $dismissible);
        }

        protected static function printNotice($content, $class = '', $dismissible = true) {
            if($dismissible === true) {
                $class .= ' is-dismissible';
            }
            ?>
            <div class="notice<?php echo $class; ?>"><p><?php echo $content; ?></p></div>
            <?php
        }

        protected static function printProcessForm($filename) {
            ?>
            <form class="fb2wp-form" method="post" action="">
                <p class="fb2wp-text">We are ready to import datas from this file:<br /> <code><?php echo $filename; ?></code></p>
                <input id="<?php echo self::IMPORT_AS_DRAFT_ID; ?>" name="<?php echo self::IMPORT_AS_DRAFT_ID; ?>" type="checkbox" value="1">
                <label for="<?php echo self::IMPORT_AS_DRAFT_ID; ?>">Import your posts as drafts</label>
                <p class="fb2wp-text">If it's ok for you, click on the button to launch the import process. It can take a while, you should get some fresh air!</p>
                <button class="button button-primary" type="submit" name="<?php echo self::BUTTON_NAME; ?>">Import!</button>
            </form>
            <?php
        }
    }
    FacebookExport2Wp::init();
}
