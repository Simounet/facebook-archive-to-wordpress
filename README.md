# Facebook Export 2 WordPress

## Description

Privacy is important. You want to leave Facebook without loosing the content previously generated. This plugin helps your to import your Facebook's posts to your WordPress instance.

## Requirements

You need to have access to your WordPress instance filesystem to upload the ZIP file from Facebook. If it's a problem for you, feel free to open an issue!

## How To

The big picture of it is:

1. Install and activate this plugin
2. Grab your content from the Facebook export feature
3. Backup your database (we will import a lot of content, if you want to revert it, the only way at the moment is to use this backup)
4. Follow the steps explained by this plugin page


## Contribute

You want to contribute to this plugin? You can submit a merge request.
