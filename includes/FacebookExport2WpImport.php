<?php

class FacebookExport2WpImport {
    const PLUGIN_REF = 'facebookexport2wp';
    const MISSING_FILENAME = 'missing';
    const DUPLICATED_FILENAME = 'duplicated';
    const POST_META_ID = 'facebook_timestamp';
    const POST_TAG_NAME = 'Facebook';
    const IMAGETYPE_ALLOWED = [
        IMAGETYPE_GIF,
        IMAGETYPE_JPEG,
        IMAGETYPE_PNG,
        IMAGETYPE_BMP
    ];

    private $uploadDir = "";
    private $dataDir = "";
    private $missingFile = "";
    private $duplicatedFile = "";
    private $uploadBaseDir = "";
    private $uploadBaseUrl = "";

    public function __construct($uploadDir, $dataDir) {
        $this->uploadDir = $uploadDir;
        $this->dataDir = $dataDir;
        $this->missingFile = $this->uploadDir . '/' . self::MISSING_FILENAME;
        $this->duplicatedFile = $this->uploadDir . '/' . self::DUPLICATED_FILENAME;
        $wpUploadDir = wp_upload_dir();
        $this->uploadBaseDir = $wpUploadDir['basedir'];
        $this->uploadBaseUrl = $wpUploadDir['baseurl'];
    }

    public function importPosts($json, $importAsDrafts = false) {
        $postStatus = $importAsDrafts ? 'draft' : 'publish';
        $this->cleanup();
        $datas = json_decode($json);
        $count = 0;

        require_once(ABSPATH . 'wp-admin/includes/image.php');
        require_once(ABSPATH . 'wp-admin/includes/file.php');
        require_once(ABSPATH . 'wp-admin/includes/media.php');

        foreach(array_reverse($datas) as $data) {
            $timestamp = $data->timestamp;
            $id = $timestamp;
            $title = $this->getTitle($data);
            $description = $this->getDescription($data, $title);
            if(!$id) {
                echo 'ID missing:';
                echo '<pre>' . print_r( $data, true ) . '</pre>'; exit;
            }
            if(!$title) {
                $this->logMissing($id, 'TITLE');
                continue;
            }

            if(!$this->postExists($id)) {
                error_reporting(E_ERROR | E_PARSE);
                $attachments = $data->attachments;
                list($medias, $externalContext) = $this->getAttachments($attachments);
                $skipPost = $this->skipPostNeeded($externalContext);
                if($skipPost) {
                    continue;
                }
                $args = array(
                    'post_title' => $title,
                    'post_content' => $description,
                    'post_status' => $postStatus,
                    'tags_input'    => [self::POST_TAG_NAME],
                    'post_date_gmt' => date('Y-m-d H:i:s', $timestamp)
                );
                $postId = wp_insert_post($args);
                $this->addPostMeta($postId);

                $attachments = $data->attachments;
                if(count($attachments) > 0) {
                    $medias = [];
                    $externalContext = [];
                    foreach($data->attachments as $els) {
                        foreach($els->data as $el) {
                            $key = key($el);
                            if($key === 'media') {
                                $medias[] = $el->media;
                            }
                            if($key === 'external_context') {
                                $externalContext[] = $el->external_context;
                            }
                        }
                    }
                    if($medias) {
                        $mediasContent = $this->mediasImport($medias, $timestamp, $postId);
                        $content = $description . implode($mediasContent);
                        $this->updatePost($postId, $content);
                    }
                    if($externalContext && $externalContext[0] && $externalContext[0]->url) {
                        $content = $description . $this->makeWpParagraph(' <a href="' . $externalContext[0]->url . '">' . $externalContext[0]->url . '</a>');
                        $this->updatePost($postId, $content);
                    }
                }
                add_post_meta($postId, self::POST_META_ID, $id, true);
                $count++;
            } else {
                $this->logDuplicated($id, $title);
            }
        }

        return $count;
    }

    private function skipPostNeeded($elements)
    {
        foreach($elements as $element) {
            if(isset($element->source)) {
                return true;
            }
        };
        return false;
    }

    private function getAttachments($attachments)
    {
        $medias = [];
        $externalContext = [];
        if(count($attachments) > 0) {
            foreach($attachments as $els) {
                foreach($els->data as $el) {
                    if(key($el) === 'media') {
                        $medias[] = $el->media;
                    }
                    if(key($el) === 'external_context') {
                        $externalContext[] = $el->external_context;
                    }
                }
            }
        }
        return array($medias, $externalContext);
    }

    private function mediasImport($medias, $timestamp, $postId)
    {
        $date = '/' . date('Y/m', $timestamp) . '/';
        $postUploadBaseDir = $this->uploadBaseDir . $date;
        wp_mkdir_p($postUploadBaseDir);
        return array_map( function ($media) use ($postId, $postUploadBaseDir, $date)
            {
                $originalDir = $this->dataDir . '/' . $media->uri;
                $pathExploded = explode('/', $originalDir);
                $filename = end($pathExploded);

                $validMedia = $this->isValidMedia($originalDir);
                if ($validMedia) {
                    $fileUrl = $postUploadBaseDir . $filename;
                    $filetype = wp_check_filetype($fileUrl);

                    if(!copy($originalDir, $fileUrl)) {
                        echo 'Failed to copy file:<br />';
                        echo '<pre>' . print_r( $originalDir . ' -> ' . $fileUrl, true ) . '</pre>'; exit;
                    }

                    $attachment = array(
                        'guid' => $postId . '/' . basename( $fileUrl ),
                        'post_mime_type' => $filetype['type'],
                        'post_parent' => $postId,
                        'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
                        'post_content' => '',
                        'post_status' => 'inherit'
                    );
                    $attachmentId = wp_insert_attachment( $attachment, $fileUrl);
                    $this->addPostMeta($attachmentId);

                    $attachmentData = wp_generate_attachment_metadata( $attachmentId, $fileUrl );
                    wp_update_attachment_metadata( $attachmentId, $attachmentData );
                    $mediaCaption = $this->getMediaCaption($media->title, $media->description);
                    $content = $attachmentData['fileformat'] === 'mp4' ?
                        $this->getVideo($attachmentData, $this->uploadBaseUrl . $date . $filename, $mediaCaption)
                        : $this->getImage($attachmentData, $this->uploadBaseUrl . $date, $mediaCaption);
                    if(isset($media->comments)) {
                        $this->insertComments($media->comments, $postId);
                    }
                    return $content;
                }
            }, $medias);
    }

    private function addMeta($id, $type = 'post')
    {
        $method = $type == 'comment' ? 'add_comment_meta' : 'add_post_meta';
        $method($id, 'importedfrom', self::PLUGIN_REF, true);
    }

    private function addPostMeta($id) {
        $this->addMeta($id);
    }

    private function addCommentMeta($id) {
        $this->addMeta($id, 'comment');
    }

    private function getMediaCaption($title, $description)
    {
        $items = array_filter([$this->stringDecoded($title), $this->stringDecoded($description)]);
        return implode(' - ', $items);
    }

    private function insertComments($comments, $postId)
    {
        array_map(function($comment) use ($postId) {
            $commentData = [
                'comment_author' => $comment->author,
                'comment_content' =>  $this->stringDecoded($comment->comment),
                'comment_date' => get_date_from_gmt(date('Y-m-d H:i:s', $comment->timestamp)),
                'comment_date_gmt' => date('Y-m-d H:i:s', $comment->timestamp),
                'comment_post_ID' => $postId
            ];
            $commentId = wp_insert_comment($commentData);
            $this->addCommentMeta($commentId);
        }, $comments);
    }

    private function getImage($attachmentData, $uploadsPostUrl, $caption) {
        $thumb = $this->getThumb($attachmentData['sizes']);
        ob_start();
        ?>
        <a href="<?php echo $this->uploadBaseUrl . '/' . $attachmentData['file']; ?>">
            <img src="<?php echo $uploadsPostUrl  . $thumb['file']; ?>" alt="" class="wp-image-2" width="<?php echo $thumb['width']; ?>" height="<?php echo $thumb['height']; ?>"/>
        </a>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $this->getFigure($content, $caption);
    }

    private function getVideo($attachmentData, $fileUrl, $caption) {
        ob_start();
        ?>
        <video width="<?php echo $attachmentData['width']; ?>" height="<?php echo $attachmentData['height']; ?>" src="<?php echo $fileUrl; ?>" controls></video>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $this->getFigure($content, $caption);
    }

    private function getFigure($content, $caption = false) {
        $captionHtml = $caption ? '<figcaption>' . $caption . '</figcaption>' : '';
        ob_start();
        ?>
        <figure class="wp-block-image size-large is-resized">
            <?php
                echo $content;
                echo $captionHtml;
            ?>
        </figure>
        <?php
        return ob_get_clean();
   }

    private function getThumb($sizes) {
        foreach(['medium_large', 'medium', 'thumbnail'] as $size) {
            if(isset($sizes[$size])) {
                return $sizes[$size];
            }
        }
    }

    private function updatePost($postId, $content)
    {
        $updatedArgs = [
            'ID' => $postId,
            'post_content' => $content
        ];
        wp_update_post($updatedArgs);
    }

    private function makeWpParagraph($content) {
        if(strlen($content) === 0) {
            return '';
        }
        ob_start();
        ?>
        <!-- wp:paragraph -->
        <p><?php echo $content; ?></p>
        <!-- /wp:paragraph -->
        <?php
        return ob_get_clean();
    }

    private function getDescription($post, $title) {
        $description = "";
        if($post->data && $post->data[0] && $post->data[0]->post) {
            $description = $this->stringDecoded($post->data[0]->post);
        }
        if($post->attachments) {
            foreach($post->attachments as $els) {
                foreach($els->data as $el) {
                    if(key($el) === 'text') {
                        if($el->text) {
                            $description .= ' ' . $el->text;
                        }
                    }
                }
            }
        }
        $description = str_replace($title, '', $description);
        if(preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $description, $matches)) {
            foreach($matches[0] as $match) {
                $description = str_replace($match, '<a href="' . $match . '" rel="noreferrer">' . $match . '</a>', $description);
            }
        }
        return $this->makeWpParagraph($description);
    }

    private function postExists($id) {
      $args = array(
          'post_type' => 'post',
          'posts_per_page' => -1,
          'meta_query' => array(
              array(
                  'key' => self::POST_META_ID,
                  'value' => $id
              )
          )
      );

      $post_query = new WP_Query( $args );
      return $post_query->have_posts();
    }

    private function isValidMedia($path) {
        $fileExtension = end(explode('.', $path));
        if($fileExtension === 'mp4') {
            return true;
        }
        $a = getimagesize($path);
        $type = $a[2];
        if(in_array($type , self::IMAGETYPE_ALLOWED)) {
            return true;
        }
        return false;
    }

    private function stringDecoded($str) {
        $pattern = '/@\[\d+:\d+:([^\]]*)\]/';
        return preg_replace($pattern, '$1', utf8_decode($str));
    }

    private function getTitle($post) {
        if($post->title) {
            return $this->stringDecoded($post->title);
        }
        if($post->data && $post->data[0] && $post->data[0]->post) {
            return $this->stringDecoded($post->data[0]->post);
        }
        if($post->attachments) {
            foreach($post->attachments as $els) {
                foreach($els->data as $el) {
                  if(key($el) === 'media') {
                      $media = $el->media;
                      if($media->title && $media->title !== 'Mobile Uploads') {
                          return $this->stringDecoded($media->title);
                      }
                      if($media->description) {
                          return $this->stringDecoded($media->description);
                      }
                  }
                }
            }
        }
    }

    private function cleanup() {
        $this->removeFile($this->missingFile);
        $this->removeFile($this->duplicatedFile);
    }

    private function logDuplicated($id, $title) {
        $this->debugLog($this->duplicatedFile, $id . ' ' . $title);
    }

    private function logMissing($id, $title) {
        $this->debugLog($this->missingFile, $id . ' ' . $title);
    }

    private function removeFile($file) {
        if(file_exists($file)) {
            unlink($file);
        }
    }

    private function debugLog($file, $content) {
        file_put_contents($file, $content . PHP_EOL, FILE_APPEND);
    }
}
