<?php

class FacebookExport2WpZip {
    const UNAUTHORIZED = 401;

    protected $datasDir = "";
    protected $uploadDir = "";

    public function __construct($uploadDir, $datasDir)
    {
        if (!class_exists('ZipArchive')) {
            throw new Exception('You need to install the ZIP PHP extension.');
        }
        $this->datasDir = $datasDir;
        $this->uploadDir = $uploadDir;
    }

    public function fileHandling($formButtonName)
    {
        if(file_exists($this->datasDir)) {
            return true;
        }
        $files = array_diff(scandir($this->uploadDir), array('.','..'));
        $zipFiles = array_filter($files, function($file) {
            $extension = end(explode('.', $file));
            return $extension === 'zip';
        });
        $this->checkFileValidity($zipFiles);
        $zipFilename = current($zipFiles);
        if(!isset($_POST[$formButtonName])) {
            throw new Exception($zipFilename, self::UNAUTHORIZED);
        }
        $zipFile = $this->uploadDir . '/' . $zipFilename;
        return $this->extractZipFile($zipFile);
    }

    private function extractZipFile($zipFile)
    {
        $zip = new ZipArchive();
        $res = $zip->open($zipFile);
        if ($res !== true) {
            throw new Exception('Something went wrong on opening this ZIP file: <code>' . $zipFile . '</code>.');
        }
        $zip->extractTo($this->datasDir);
        $zip->close();
        return true;
    }

    private function checkFileValidity($zipFiles)
    {
        return
            $this->isFileCountValid($zipFiles) &&
            $this->isFileMimeTypeValid($zipFiles);
    }

    private function isFileCountValid($zipFiles)
    {
        $zipFilesCount = count($zipFiles);
        if($zipFilesCount === 0) {
            throw new Exception('No ZIP file detected, you should upload yours at <code>' . $this->uploadDir . '</code>.');
        }
        if($zipFilesCount > 1) {
            throw new Exception('We detected more than 1 ZIP file. You should keep only the one you want to import.');
        }
        return true;
    }

    private function isFileMimeTypeValid($zipFiles)
    {
        $zipFile = $this->uploadDir . '/' . current($zipFiles);
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $zipFileMimeType = finfo_file($finfo, $zipFile);
        finfo_close($finfo);
        if($zipFileMimeType !== 'application/zip') {
            throw new Exception('The ZIP file uploaded doesn\'t seems to be a valid ZIP file. Maybe something went wrong on upload, try again.');
        }
        return true;
    }
}
